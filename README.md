# website

This repository contains the source code for [scybernaut.me](https://scybernaut.me).

## Building the source

1. Download dependencies using npm or yarn

   ```sh
   yarn init
   ```

2. Run the build script
   ```sh
   yarn build
   ```

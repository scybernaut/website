module.exports = {
  content: ["./src/**/*.svelte"],
  theme: {
    fontFamily: {
      body: ["Inter", "sans-serif"],
      default: "sans-serif",
      mono: "monospace",
    },
    extend: {
      maxWidth: {
        "4/5": "80%",
      },
      colors: {
        dark: "#1a1a1a",
        green: "#00d455",
        light: "#f9f9f9",
        "light-darker": "#e3e3e3",
      },
    },
  },
  plugins: [],
};
